module leaf_m
    use iso_varying_string, only: varying_string, assignment(=)
    use tree_m, only: tree_t

    implicit none
    private
    public :: leaf_t

    type, extends(tree_t) :: leaf_t
        private
        type(varying_string) :: description
    contains
        private
        procedure, public :: to_string
    end type

    interface leaf_t
        module procedure constructor
    end interface
contains
    pure function constructor(description) result(leaf)
        character(len=*), intent(in) :: description
        type(leaf_t) :: leaf

        leaf%description = description
    end function

    pure function to_string(self) result(string)
        class(leaf_t), intent(in) :: self
        type(varying_string) :: string

        string = self%description
    end function
end module

program main
    use iso_varying_string, only: put_line
    use leaf_m, only: leaf_t
    use node_m, only: node_t
    use tree_item_m, only: tree_item_t

    implicit none

    type(leaf_t) :: leaf
    type(node_t) :: straight_tree
    type(node_t) :: simple_tree
    type(node_t) :: complex_tree
    type(node_t) :: crazy_tree

    leaf = leaf_t("leaf")
    straight_tree = node_t( &
            "parent", &
            [ tree_item_t(leaf_t("child")) &
            ])
    simple_tree = node_t( &
            "parent", &
            [ tree_item_t(leaf_t("child1")) &
            , tree_item_t(leaf_t("child2")) &
            ])
    complex_tree = node_t( &
            "parent", &
            [ tree_item_t(node_t( &
                    "child1", &
                    [ tree_item_t(leaf_t("grandchild1")) &
                    , tree_item_t(leaf_t("grandchild2")) &
                    ])) &
            , tree_item_t(node_t( &
                    "child2", &
                    [ tree_item_t(leaf_t("grandchild3")) &
                    , tree_item_t(leaf_t("grandchild4")) &
                    ])) &
            ])
    crazy_tree = node_t( &
            "parent", &
            [ tree_item_t(node_t( &
                    "child1", &
                    [ tree_item_t(leaf_t("grandchild1")) &
                    , tree_item_t(leaf_t("grandchild2")) &
                    ])) &
            , tree_item_t(node_t( &
                    "child2", &
                    [ tree_item_t(node_t( &
                            "grandchild3", &
                            [ tree_item_t(leaf_t("greatgrandchild1")) &
                            , tree_item_t(leaf_t("greatgrandchild2")) &
                            ])) &
                    ])) &
            , tree_item_t(node_t( &
                    "child3", &
                    [ tree_item_t(leaf_t("grandchild5")) &
                    , tree_item_t(leaf_t("grandchild6")) &
                    ])) &
            ])
    call put_line(leaf%to_string())
    call put_line(straight_tree%to_string())
    call put_line(simple_tree%to_string())
    call put_line(complex_tree%to_string())
    call put_line(crazy_tree%to_string())
end program
